<?php


namespace console\controllers;
use common\constants\StatusAppleConstants;
use common\models\Color;
use common\models\StatusApple;
use common\models\User;
use Yii;
use yii\console\Controller;
use yii\db\Exception;

class TestDataController extends Controller
{
    /**
     * Заполнение базовыми данными бд
     * @return bool
     * @throws \Exception
     */
    public function actionCreateData()
    {
        try {
            $this->createStatuses();
            $this->createColor();
            $this->createUser();
        }
        catch (\Exception $e){
           echo 'Ошибка при выполнении: '.$e->getMessage().' Код: '.$e->getCode();
           return false;
        }
        echo 'done!';
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function createStatuses()
    {
        try {
            foreach ($this->getStatuses() as $statusData){
                $status = new StatusApple();
                $status->name = $statusData['name'];
                $status->id = $statusData['id'];
                $status->save();
            }
        }
        catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
        echo "Статусы добавлены".PHP_EOL;
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function createColor()
    {
        try {
            foreach ($this->getColor() as $colorData){
                $color = new Color();
                $color->name = $colorData['name'];
                $color->color = $colorData['color'];
                $color->save();
            }
        }
        catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
        echo "Цвета добавлены".PHP_EOL;
        return true;
    }

    /**
     * @return bool
     * @throws Exception
     */
    protected function createUser()
    {
        $username = 'test';
        $password = 'apple';
        $email = 'test@test.com';
        try {
            $user = new User();
            $user->username = $username;
            $user->email = $email;
            $user->status = User::STATUS_ACTIVE;
            $user->generateAuthKey();
            $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($password);
            $user->save();
        }
        catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
        echo "Тестовый пользователь добавлен! Имя: $username Пароль: $password".PHP_EOL;
        return true;
    }

    /**
     * @return array[]
     */
    protected function getStatuses()
    {
        return
            [
                [
                    'name'=>'На дереве',
                    'id'=> StatusAppleConstants::ON_THE_TREE
                ],
                [
                    'name'=>'Упало',
                    'id'=> StatusAppleConstants::FELL
                ],
                [
                    'name'=>'Испорчено',
                    'id'=> StatusAppleConstants::SPOILED
                ],
                [
                    'name'=>'Съедено',
                    'id'=> StatusAppleConstants::EATEN
                ],

            ];
    }

    /**
     * @return array
     */
    protected function getColor()
    {
        return
            [
                [
                    'name'=>'Черный',
                    'color'=>'000000'
                ],
                [
                    'name'=>'Красный',
                    'color'=> 'ff0000'
                ],
                [
                    'name'=>'Зеленый',
                    'color'=>'66ff00'
                ],
            ];
    }


}
