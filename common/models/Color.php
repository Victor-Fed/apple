<?php

namespace common\models;

use Yii;
use yii\db\Exception;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "color".
 *
 * @property int $id
 * @property string|null $name  Цвет яблока
 * @property string|null $color hex
 *
 * @property Apple[] $apples
 */
class Color extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'color';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['color'], 'string', 'max' => 6],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
        ];
    }

    /**
     * Gets query for [[Apples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApples()
    {
        return $this->hasMany(Apple::class, ['color_id' => 'id']);
    }

    /**
     * Возвращает случайный id цвета
     * @return int
     * @throws NotFoundHttpException
     */
    public static function getRandomColor(): int
    {
        $color  = Color::find()->select("id")->asArray()->all();
        if(empty($color))
            throw new NotFoundHttpException('Не найдено ни одного цвета в БД');
        $randomKey = array_rand($color);
        return $color[$randomKey]['id'];
    }
}
