# Написать класс/объект Apple
Написать класс/объект Apple с хранением яблок в БД MySql следуя ООП парадигме
## Системные требования

Для запуска приложения требуется:

-   `docker >= 18.0` _(install: `curl -fsSL get.docker.com | sudo sh`)_
-   `docker-compose >= 3` _([installing manual](https://docs.docker.com/compose/install/#install-compose))_
-   `git`
- `composer`


### Запуск приложения

Выполните следующие команды:

```
$ git clone https://Victor-Fed@bitbucket.org/Victor-Fed/apple.git
$ cd apple
$ docker-compose up -d
$ docker-compose  exec  backend composer install
$ docker-compose  exec  backend php ./init  
```
Далее укажите в файле common\config\main-local.php конфиги для db(указаны в docker-compose.yml), пример:
```
'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=mysql;dbname=yii2advanced',
            'username' => 'yii2advanced',
            'password' => 'secret',
            'charset' => 'utf8',
        ],
)
```
Далее выполните следующие команды:
```
$ docker-compose  exec  backend php ./yii migrate 
$ docker-compose  exec  backend php ./yii test-data/create-data
```
Приложение будет доступно по [localhost:21080](http://localhost:21080) 
Логин и пароль для тестового юзера:  
test   
apple
