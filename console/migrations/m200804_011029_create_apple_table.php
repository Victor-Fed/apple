<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apple}}`.
 */
class m200804_011029_create_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%apple}}', [
            'id' => $this->primaryKey(),
            'color_id' => $this->integer()->comment('Цвет яблока'),
            'date_create' => $this->timestamp()->notNull(),
            'date_of_fall' => $this->timestamp()->defaultValue(null)->comment('Дата падения яблока'),
            'status_id' => $this->integer()->comment('Статус яблока'),
            'percent_eat' => $this->decimal(3,2)->defaultValue(0)
        ],$tableOptions);

        $this->createIndex('idx_task_status_id','{{%apple}}','status_id');
        $this->createIndex('idx_task_color_id','{{%apple}}','color_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apple}}');
    }
}
