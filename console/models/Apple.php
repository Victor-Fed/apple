<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property int|null $color_id Цвет яблока
 * @property string|null $date_create Дата создания
 * @property string|null $date_of_fall Дата падения яблока
 * @property int|null $status_id Статус яблока
 * @property float|null $percent_eat
 *
 * @property Color $color
 * @property StatusApple $status
 */
class Apple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color_id', 'status_id'], 'integer'],
            [['date_create', 'date_of_fall'], 'safe'],
            [['percent_eat'], 'number'],
            [['color_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::className(), 'targetAttribute' => ['color_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusApple::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color_id' => 'Color ID',
            'date_create' => 'Date Create',
            'date_of_fall' => 'Date Of Fall',
            'status_id' => 'Status ID',
            'percent_eat' => 'Percent Eat',
        ];
    }

    /**
     * Gets query for [[Color]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::className(), ['id' => 'color_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusApple::className(), ['id' => 'status_id']);
    }
}
