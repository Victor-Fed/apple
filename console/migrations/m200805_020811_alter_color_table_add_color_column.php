<?php

use yii\db\Migration;

/**
 * Class m200805_020811_alter_color_table_add_color_column
 */
class m200805_020811_alter_color_table_add_color_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('color','color',$this->char(6)->defaultValue('000000'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('color','color');
    }

}
