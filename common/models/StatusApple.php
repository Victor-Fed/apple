<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "status_apple".
 *
 * @property int $id
 * @property string|null $name Название статуса
 *
 * @property Apple[] $apples
 */
class StatusApple extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status_apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Apples]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApples()
    {
        return $this->hasMany(Apple::className(), ['status_id' => 'id']);
    }
}
