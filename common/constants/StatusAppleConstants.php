<?php


namespace common\constants;


class StatusAppleConstants
{
    const ON_THE_TREE = 1;
    const FELL = 2;
    const SPOILED = 3;
    const EATEN = 4;
}
