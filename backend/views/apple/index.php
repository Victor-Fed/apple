<?php
/* @var $this yii\web\View */
/* @var $apples array */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$countApple = count($apples)
?>
<a type="button" href="/apple/create" class="btn btn-success">Сгенерировать еще яблок</a>
<?php if(count($apples) > 0): ?>
<p>Найдено яблок: <?=$countApple?> </p>
<table class="table table-bordered apple-table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Состояние</th>
        <th scope="col">Цвет</th>
        <th scope="col">Дата появления</th>
        <th scope="col">Дата падения</th>
        <th class="color-change" scope="col">Сколько съели(%)</th>
        <th scope="col">Действия</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($apples as $apple):?>
    <tr id="id-<?=$apple['id']?>">
        <th scope="row"><?=$apple['id']?></th>
        <td><?=$apple['status']['name'] ?? 'Не определен'?></td>
        <td style="color:<?='#'.$apple['color']['color']?>"><?=$apple['color']['name'] ?? 'Не определен'?></td>
        <td><?=$apple['date_create']?></td>
        <td><?=($apple['date_of_fall'] === null) ? '-' : $apple['date_of_fall']  ?></td>
        <td><?=(float)$apple['percent_eat']*100?></td>
        <td>
            <?php $form = ActiveForm::begin(['id' => 'eat-apple-form', 'action'=>"/apple/eat?id=".$apple['id']]); ?>
            <label for="example-number-input" class="col-2 col-form-label">Сколько съесть(%)</label>
            <div class="col-10">
                <input class="form-control" type="number" name="percent" max="100" min="0" value="0" id="eat_percent">
            </div>
                <?= Html::submitButton('Съесть', ['class' => 'btn btn-success', 'name' => 'apple-eat-button']) ?>
            <?php ActiveForm::end(); ?>
            <a type="button" class="btn btn-primary" href="/apple/fall?id=<?=$apple['id']?>">Уронить</a>

            <a type="button" class="btn btn-danger" href="/apple/delete?id=<?=$apple['id']?>">Удалить</a>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php else: ?>
<h4>Яблок не найдено</h4>
<?php endif;?>

<style>
.apple-table {
    margin-top: 10px;
}
.btn {
    margin-top: 5px;
}

</style>

<?php
$js = <<<JS
$(function() {
    
       let hash = location.hash
       if(hash != ''){
         let id =  hash.replace('#', '')
         $('#'+id).addClass('info')
       }
       
    });
JS;
$this->registerJs(
    $js,
    View::POS_READY,
    'apple-script'
);
?>
<script>



</script>
