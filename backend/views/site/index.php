<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Тестовое задание</h1>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12 center-block">
                <p><span style="font-weight: 400;">Установить advanced шаблон Yii2 фреймворка, в backend-приложении реализовать следующий закрытый функционал (доступ в backend-приложение должен производиться только по паролю, сложного разделения прав не требуется):</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">Написать класс/объект Apple с хранением яблок в БД MySql следуя ООП парадигме.</span></p>
                <p>&nbsp;</p>
                <p><strong>Функции</strong></p>
                <p><span style="font-weight: 400;">- упасть</span></p>
                <p><span style="font-weight: 400;">- съесть ($percent - процент откушенной части)</span></p>
                <p><span style="font-weight: 400;">- удалить (когда полностью съедено)</span></p>
                <p>&nbsp;</p>
                <p><strong>Переменные</strong></p>
                <p><span style="font-weight: 400;">- цвет (устанавливается при создании объекта случайным)</span></p>
                <p><span style="font-weight: 400;">- дата появления (устанавливается при создании объекта случайным unixTmeStamp)</span></p>
                <p><span style="font-weight: 400;">- дата падения (устанавливается при падении объекта с дерева)</span></p>
                <p><span style="font-weight: 400;">- статус (на дереве / упало)</span></p>
                <p><span style="font-weight: 400;">- сколько съели (%)</span></p>
                <p><span style="font-weight: 400;">- другие необходимые переменные, для определения состояния.</span></p>
                <p>&nbsp;</p>
                <p><strong>Состояния</strong></p>
                <p><span style="font-weight: 400;">- висит на дереве</span></p>
                <p><span style="font-weight: 400;">- упало/лежит на земле</span></p>
                <p><span style="font-weight: 400;">- гнилое яблоко</span></p>
                <p>&nbsp;</p>
                <p><strong>Условия</strong></p>
                <p><span style="font-weight: 400;">Пока висит на дереве - испортиться не может.</span></p>
                <p><span style="font-weight: 400;">Когда висит на дереве - съесть не получится.</span></p>
                <p><span style="font-weight: 400;">После лежания 5 часов - портится.</span></p>
                <p><span style="font-weight: 400;">Когда испорчено - съесть не получится.</span></p>
                <p><span style="font-weight: 400;">Когда съедено - удаляется из массива яблок.</span></p>
                <p>&nbsp;</p>
                <p><strong>Пример результирующего скрипта:</strong></p>
                <p><span style="font-weight: 400;">$apple = new Apple('green');</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">echo $apple-&gt;color; // green</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">$apple-&gt;eat(50); // Бросить исключение - Съесть нельзя, яблоко на дереве</span></p>
                <p><span style="font-weight: 400;">echo $apple-&gt;size; // 1 - decimal</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">$apple-&gt;fallToGround(); // упасть на землю</span></p>
                <p><span style="font-weight: 400;">$apple-&gt;eat(25); // откусить четверть яблока</span></p>
                <p><span style="font-weight: 400;">echo $apple-&gt;size; // 0,75</span></p>
                <p>&nbsp;</p>
                <p><span style="font-weight: 400;">На странице в приложении должны быть отображены все яблоки, которые на этой же странице можно сгенерировать в случайном кол-ве соответствующей кнопкой.</span></p>
                <p><span style="font-weight: 400;">Рядом с каждым яблоком должны быть реализованы кнопки или формы соответствующие функциям (упасть, съесть&nbsp; процент&hellip;) в задании.</span></p>
                <p><span style="font-weight: 400;">Задача не имеет каких-либо ограничений и требований. Все подходы к ее решению определяют способность выбора правильного алгоритма при проектировании системы и умение предусмотреть любые возможности развития алгоритма. Задание должно быть выложено в репозиторий на gitHub, с сохранением истории коммитов. Креативность только приветствуется.</span></p>


            </div>
        </div>

    </div>
</div>
