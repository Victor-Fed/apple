<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%status_apple}}`.
 */
class m200804_012522_create_status_apple_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%status_apple}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(null)->comment('Название статуса'),
        ],$tableOptions);

        $this->addForeignKey('fk-apple-status_id','{{%apple}}','status_id','status_apple','id','SET NULL','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apple-status_id','apple');
        $this->dropTable('{{%status_apple}}');
    }
}
