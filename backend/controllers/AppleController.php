<?php

namespace backend\controllers;

use common\constants\StatusAppleConstants;
use common\models\Apple;
use common\models\Color;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;

class AppleController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     *
     */
    public function actionIndex()
    {
        //Проверяем сколько висят яблоки
        Apple::fallAppleUpdate();
        //Возвращаем список яблок
        $model = Apple::find()->joinWith(['status','color'])->orderBy('id DESC')->asArray()->all();
        return $this->render('index',[
            'apples'=>$model,
        ]);
    }

    /**
     * Сгенерировать случайное количество яблок
     * @return \yii\web\Response
     *
     */
    public function actionCreate()
    {
        $count = rand(1,5);
        for ($i = 0; $i<=$count; $i++){
            $apple = new Apple();
            $apple->color_id = Color::getRandomColor();
            $apple->status_id = StatusAppleConstants::ON_THE_TREE;
            $apple->date_create = date('Y-m-d H:i:s');
            $apple->save();
        }
        return $this->redirect('/apple/index');
    }

    /**
     * Уронить яблоко
     * @param $id
     * @throws HttpException
     * @throws NotFoundHttpException
     *
     */
    public function actionFall($id)
    {
        $model = $this->findModel($id);
        $model->fallToGround();
        if($model->save()){
            $this->redirect("/apple/index#id-$model->id");
        }
        else
            throw new HttpException('500', 'Не удалось изменить статус');
    }

    /**
     * Съесть яблоко
     * @param $id int
     * @return \yii\web\Response
     * @throws HttpException
     */
    public function actionEat($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $percentEat = (int)$request->post('percent');
        $model->eatApple($percentEat);
        if($model->save()){
            $this->redirect("/apple/index#id-$model->id");
        }
        else {
            throw new HttpException('500',$model->errors);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws HttpException
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     *
     */
    public function actionDelete($id)
    {
        $model =  $this->findModel($id);
        if(in_array($model->status_id, [StatusAppleConstants::EATEN, StatusAppleConstants::SPOILED])){
            $model->delete();
        }
        else
            throw new HttpException('400','Удалить можно только съеденое или испорченное яблоко');
        return $this->redirect("/apple/index");
    }

    /**
     * @param $id
     * @return Apple|null
     * @throws NotFoundHttpException
     *
     */
    protected function findModel($id)
    {
        if (($model = Apple::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Запись не найдена');
    }


}
