<?php

namespace common\models;

use common\constants\StatusAppleConstants;
use Yii;
use yii\db\Exception;
use yii\web\HttpException;

/**
 * This is the model class for table "apple".
 *
 * @property int $id
 * @property int|null $color_id Цвет яблока
 * @property string|null $date_create Дата создания
 * @property string|null $date_of_fall Дата падения яблока
 * @property int|null $status_id Статус яблока
 * @property float|null $percent_eat
 *
 * @property Color $color
 * @property StatusApple $status
 */
class Apple extends \yii\db\ActiveRecord
{

    /**
     * Сколько часов пролежит яблоко прежде чем испортится
     * @var float|int
     */
    private static $timeFromSpoiled = 5 * 3600;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apple';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color_id', 'status_id'], 'integer'],
            [['date_create', 'date_of_fall'], 'safe'],
            [['percent_eat'], 'number'],
            [['color_id'], 'exist', 'skipOnError' => true, 'targetClass' => Color::class, 'targetAttribute' => ['color_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusApple::class, 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color_id' => 'Color ID',
            'date_create' => 'Date Create',
            'date_of_fall' => 'Date Of Fall',
            'status_id' => 'Status ID',
            'percent_eat' => 'Percent Eat',
        ];
    }

    /**
     * Обновляет статус у  яблок, которые должны испортится
     * @return int
     * @throws Exception
     */
    public static function fallAppleUpdate()
    {
        $sql = "UPDATE ".self::tableName().
                " SET status_id =  ".StatusAppleConstants::SPOILED.
               " WHERE TIMESTAMPDIFF(SECOND ,date_of_fall, CURRENT_TIMESTAMP) > ".self::$timeFromSpoiled.
        " AND status_id = ".StatusAppleConstants::FELL.
        " AND date_of_fall IS NOT NULL";
        return Yii::$app->db->createCommand($sql)
            ->execute();
    }

    /**
     * Сколько процентов съедено
     * @param int $percentEatPost
     * @return $this
     * @throws HttpException
     */
    public function eatApple(int $percentEatPost):Apple
    {
        if($this->status_id === StatusAppleConstants::FELL) {
            $percentEat = round($percentEatPost/100,2);
            $percentSum = $this->percent_eat + $percentEat;
            $this->percent_eat = ($percentSum > 1) ? 1: $percentSum;
            $this->status_id = ($percentSum < 1) ? $this->status_id : StatusAppleConstants::EATEN;
            return $this;
        }
        else{
            throw new HttpException('500','Съесть можно только упавшее яблоко');
        }

    }

    /**
     * Уронить яблоко
     * @return $this
     * @throws HttpException
     */
    public function fallToGround():Apple
    {
        if($this->status_id === StatusAppleConstants::ON_THE_TREE){
            $this->status_id = StatusAppleConstants::FELL;
            $this->date_of_fall = date('Y-m-d H:i:s');
        }
        else
            throw new HttpException('500','Яблоко можно уронить только если оно на дереве!');

        return $this;
    }
    /**
     * Gets query for [[Color]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColor()
    {
        return $this->hasOne(Color::class, ['id' => 'color_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(StatusApple::class, ['id' => 'status_id']);
    }



}
