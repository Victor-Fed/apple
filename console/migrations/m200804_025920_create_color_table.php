<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%color}}`.
 */
class m200804_025920_create_color_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%color}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->defaultValue(null)->comment('Цвет яблока'),
        ],$tableOptions);

        $this->addForeignKey('fk-apple-color_id','{{%apple}}','color_id','color','id','SET NULL','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-apple-color_id','{{%apple}}');
        $this->dropTable('{{%color}}');
    }
}
